package autotests

import org.apache.spark.sql.{DataFrame}

import scala.util.{Failure, Success, Try}
import secondaryFunctions._


object tests {

  // Тест на количество
  def testCount(implicit querySource: String): (String, Boolean) = {
    
    val numOfTest = findTestNum //получаем номер теста
    val queryTarget = getTarget //получем название target-файла
    try {
      val files = getFiles(querySource, queryTarget) // Чтение файлов
      //проверка на успешное выполнение чтения
      if (files._1 == null) return (s"Тест на количество $numOfTest: файл $querySource не удалось прочитать", false)
      if (files._2 == null) return (s"Тест на количество $numOfTest: файл $queryTarget не найден или его не удалось прочитать", false)
      // Получаем значения count
      val sourceDf = spark.sql(files._1).first().get(0)
      val targetDf = spark.sql(files._2).first().get(0)
      // Сравниваем
      if (sourceDf == targetDf) (s"Тест на количество $numOfTest", true)
      else (s"Тест на количество $numOfTest: кол-во строк различается ($sourceDf в source, $targetDf в target)", false)
    }
    catch {
      case e:Exception => (s"Тест на количество $numOfTest вызвал неизвестное исключение:\n ${e.getStackTrace.mkString("\n")}", false)}
  }

  // Тест на сравнение датафреймов
  def testArrays(implicit querySource: String): (String, Boolean) = {
    
    val numOfTest = findTestNum //получаем номер теста
    val queryTarget = getTarget //получем название target-файла
    try {
      val files = getFiles(querySource, queryTarget) // Чтение файлов
      //проверка на успешное выполнение чтения
      if (files._1 == null) return (s"Тест на соответствие $numOfTest: файл $querySource не удалось прочитать", false)
      if (files._2 == null) return (s"Тест на соответствие $numOfTest: файл $queryTarget не найден или его не удалось прочитать", false)
      val sourceDf = spark.sql(files._1).cache()
      val targetDf = spark.sql(files._2).cache()

      //Оборачиваем в Try, чтобы не возникало исключений при использовании except из-за разного кол-ва аргументов
      Try(sourceDf.exceptAll(targetDf)) match {
        case Success(value) =>
          if (value.count() == 0) {
            if (targetDf.exceptAll(sourceDf).count() == 0) (s"Тест на соответствие $numOfTest", true)
            else (s"Тест на соответствие $numOfTest: массивы данных различаются (несовпадающие строки)", false)
          }
          else
            (s"Тест на соответствие $numOfTest: массивы данных различаются (несовпадающие строки)", false)
        case Failure(exception) =>
          (s"Тест на соответствие $numOfTest: массивы данных различаются (несовпадающие атрибуты) \nsource: ${sourceDf.columns.mkString(",")} \ntarget: ${targetDf.columns.mkString(",")}", false)
      }
    }
    catch {
      case e:Exception => (s"Тест на соответствие $numOfTest вызвал неизвестное исключение:\n ${e.getStackTrace.mkString("\n")}", false)}
  }

  // Тест на константы
  def testConst(implicit querySource: String): (String, Boolean) = {
    
    val numOfTest = findTestNum //получаем номер теста
    val queryTarget = getTarget //получем название target-файла
    try {
      val files = getFiles(querySource,queryTarget) // Чтение файлов
      //проверка на успешное выполнение чтения
      if (files._1 == null) return (s"Тест на константы $numOfTest: файл $querySource не удалось прочитать", false)
      if (files._2 == null) return (s"Тест на константы $numOfTest: файл $queryTarget не найден или его не удалось прочитать", false)
      // Приводим результат запроса в нужный формат, соответствующий второму файлу, и превращаем в массивы строк
      val sourceConst = spark.sql(files._1).collect().mkString("\n").replace("[", "").replace("]", "").toLowerCase
      val targetConst = files._2.replaceAll(" ", "").toLowerCase
      //проверяем равенство двух полученных результатов и возвращаем соответствующее сообщение
      if (sourceConst == targetConst) (s"Тест на константы $numOfTest", true)
      else (s"Тест на константы $numOfTest: результат запроса не соответствует константным значениям", false)
    }
    catch {
      case e:Exception => (s"Тест на константы $numOfTest вызвал неизвестное исключение:\n ${e.getStackTrace.mkString("\n")}", false)}
  }

  // Тест на DDL
  def testDDL(implicit querySource: String): (String, Boolean) = {
    
    val numOfTest = findTestNum //получаем номер теста
    val queryTarget = getTarget //получем название target-файла
    try {
      val files = getFiles(querySource, queryTarget) // Чтение файлов
      //проверка на успешное выполнение чтения
      if (files._1 == null)
        return (s"Тест на структуру таблиц $numOfTest: файл $querySource не удалось прочитать", false)
      if (files._2 == null)
        return (s"Тест на структуру таблиц $numOfTest: файл $queryTarget не найден или его не удалось прочитать", false)
      // Аналогично с тестами на константы, приводим результат  в нужный формат для дальнейшего сравнения
      val targetDDL = spark.sql(files._2)
        .drop("comment")
        .collect().mkString("\n")
        .replace("[", "")
        .replace("]", "")
        .toLowerCase
      val sourceDDL = files._1
        .replaceAll(" ", "")
        .replaceAll(";", ",")
        .toLowerCase
      //проверяем равенство двух полученных результатов и возвращаем соответствующее сообщение
      if (targetDDL == sourceDDL) (s"Тест на схему $numOfTest", true)
      else (s"Тест на структуру таблиц $numOfTest: структура таблиц не совпадает", false)
    }
    catch {
      case e:Exception => (s"Тест на структуру $numOfTest вызвал неизвестное исключение:\n ${e.getStackTrace.mkString("\n")}", false)}
  }

  def testUnknown(implicit querySource: String): (String, Boolean) = {
    val result = testArrays
    (result._1.replace("Тест на соответствие", "Неизвестный тест"), result._2)
  }

}
