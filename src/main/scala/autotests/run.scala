package autotests

import java.util.zip.ZipInputStream

import scala.util.matching.Regex
import autotests.tests._


case class customException(message: String) extends Exception(message)

object run {

  def main(args: Array[String]) = {

    val mask = new Regex("test_\\d+_source_\\w+.sql").regex //Маска для поиска файлов с тестами
    val path = getClass.getProtectionDomain.getCodeSource.getLocation //Путь до jar-файла
    val zis = new ZipInputStream(path.openStream()) //Поток файлов из jar'ника

    //Перебор файлов потока
    val resultsTests = Stream.continually(zis.getNextEntry).takeWhile {
      case null => zis.close(); false
      case _ => true  }
      .map(_.getName) //Получаем имена файлов
      .toList
      .filter(_.matches(mask)) // Оставляем только те, которые удовлетворяют маске
      // В зависимости от названия файла вызываем необходимый метод
      .map{
        case name: String if (name.contains("counts")) =>
          testCount(name)

        case name: String if (name.contains("constants")) =>
          testConst(name)

        case name: String if (name.contains("ddl")) =>
          testDDL(name)

        // Последний кейс работает как для тестов на сравнение массивов (датафреймов), так и для неизвестных тестов
        case name: String if (name.contains("arrays")) =>
          testArrays(name)

        case name: String =>
          testUnknown(name)


      }

    val failedTests = resultsTests.filterNot(_._2) //отбираем негативные тесты

    //если есть негативные тесты, выбрасываем исключение с результатами
    if (!failedTests.isEmpty) throw new customException(s"НЕКОТОРЫЕ ТЕСТЫ НЕ ПРОЙДЕНЫ!!! \n" +
      s"${resultsTests.map{
        case (x,true) => ("Тест пройден", x);
        case (x,false) => ("Тест не пройден", x)}
        .mkString("\n\n")
      }")
    else resultsTests.foreach(println) //если все тесты успешно завершились, все равно выводим их список для проверки в Spark UI при необходимости

  }
}
