package autotests

import org.apache.spark.sql.SparkSession

import scala.util.{Failure, Success, Try}

object secondaryFunctions {
  val spark = SparkSession
    .builder
    .appName("test")
    .enableHiveSupport()
    .config("hive.metastore.uris", "thrift://dn01:9083")
    .getOrCreate()

  spark.sparkContext.setLogLevel("ERROR")

  //функция для получения содержимого файлов
  def getFiles(querySource: String, queryTarget: String) = {
    //пытаемся прочитать файл source
    val source = Try(scala.io.Source.fromInputStream(getClass.getResourceAsStream(s"/$querySource")).getLines().mkString("\n")) match {
      case Success(v) => v //если успешно - получаем содержимое
      case Failure(exception) => null //иначе помечаем как null
    }
    // Проверяем наличие target-файла и пытаемся его прочитать
    Try(scala.io.Source.fromInputStream(getClass.getResourceAsStream(s"/$queryTarget")).getLines().mkString("\n")) match {
      case Success(v) => (source, v) //в случае успешного выполнения возвращаем результат
      case Failure(exception) => (source, null) //если не удалось, то null
    }
  }
  // функция для выделения номера теста из названия файла
  def findTestNum(implicit fileName: String): String = fileName.substring(5, fileName.indexOfSlice("_source_"))

  // функция для получения названия target-файла
  def getTarget(implicit source: String): String = source.replace("source", "target")

  //рекурсивная функция для поиска различающихся строк для констант и ddl
  def loop(s: Array[String],t: Array[String],n: Int, result1: Array[String], result2: Array[String]): (Array[String], Array[String]) = {
    if (n == 0) (result1, result2) //возвращаем результат, если строки кончились
    else {
      if (s.head == t.head) loop(s.tail, t.tail, n - 1, result1, result2) //если строки одинаковые, то вызываем функцию для оставшихся строк
      else loop(s.tail, t.tail, n - 1, result1 :+ s.head, result2 :+ t.head) //иначе добавляем различающиеся строки в результат при вызове функции
    }
  }
}
